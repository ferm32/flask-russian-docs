.. _tutorial-folders:

Шаг 0: Создание папок
=====================

Перед тем, как мы начнём, давайте создадим папки, необходимые
для нашего приложения::

    /flaskr
        /static
        /templates

Папка `flaskr` не является пакетом Python, это просто некое место, куда мы
будем класть наши файлы.  Далее, в эту папку, в добавок к главному модулю, мы
поместим схему нашей базы данных.  Это будет сделано следующим образом.  Файлы
внутри папки `static` доступны пользователям приложения по протоколу HTTP.
Это место, куда попадут файлы css и javascript.  В папке `templates` Flask
будет искать шаблоны `Jinja2`_.  В эту папку будут помещены шаблоны,
которые вы будете создавать в ходе работы с этим руководством.

Продолжение: :ref:`tutorial-schema`.

.. _Jinja2: http://jinja.pocoo.org/

`Оригинал этой страницы <http://flask.pocoo.org/docs/tutorial/folders/>`_