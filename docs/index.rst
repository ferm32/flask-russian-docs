.. rst-class:: hide-header

Добро пожаловать во Flask
=========================

.. image:: _static/logo-full-rus.png
   :alt: Flask: веб-разработка капля за каплей
    :align: center
    :target: https://palletsprojects.com/p/flask/

Добро пожаловать в документацию по Flask.  Начните с раздела
:ref:`installation`, затем ознакомьтесь с разделом :ref:`quickstart`.
Есть также более детальное :ref:`tutorial`, которое покажет, как создать на
Flask маленькое, но полноценное приложение.  Общие заготовки описаны в разделе
:ref:`patterns`. Оставшаяся документация детально описывает каждый из компонентов
Flask, с точными ссылками в раздел :ref:`api`.

Flask зависит от шаблонизатора `Jinja2`_ и инструментария WSGI `Werkzeug`_.
Их документация может быть найдена здесь:

-   `Документация по Jinja2 <http://jinja.pocoo.org/docs>`_
-   `Документация по Werkzeug <http://werkzeug.pocoo.org/docs>`_

.. _Jinja2: https://www.palletsprojects.com/p/jinja/
.. _Werkzeug: https://www.palletsprojects.com/p/werkzeug/

.. include:: contents.rst.inc

Оригинал этой страницы: `<http://flask-russian-docs.readthedocs.io/>`_
Оригинал исходной страницы: `<https://flask.pocoo.org/docs/1.0/>`_

© 2012-2019 Перевод ferm32

© 2013 Перевод разделов design, ... http://vladimir-stupin.blogspot.ru/
